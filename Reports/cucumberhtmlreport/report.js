$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("feature/web/Create_Account.feature");
formatter.feature({
  "line": 1,
  "name": "Create log in account for online shopping",
  "description": "",
  "id": "create-log-in-account-for-online-shopping",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 3,
  "name": "As a user I want to create an account to log onto online shopping",
  "description": "",
  "id": "create-log-in-account-for-online-shopping;as-a-user-i-want-to-create-an-account-to-log-onto-online-shopping",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 2,
      "name": "@regression"
    }
  ]
});
formatter.step({
  "line": 4,
  "name": "the \"\u003ctitle\u003e\" is correct",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the sign in tab",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user enters valid \"\u003cemail_address\u003e\" address",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "user clicks on create an account button",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "user enters \"\u003ccustomer_firstname\u003e\" \"\u003ccustomer_lastname\u003e\" \"\u003cemail\u003e\" \"\u003cpassword\u003e\" \"\u003cdays\u003e\" \"\u003cmonths\u003e\" \"\u003cyears\u003e\" \"\u003caddress_firstname\u003e\" \"\u003caddress_lastname\u003e\" \"\u003caddress_company\u003e\" \"\u003caddress_one\u003e\" \"\u003caddress_two\u003e\" \"\u003ccity\u003e\" \"\u003cstate\u003e\" \"\u003cpostcode\u003e\" \"\u003ccountry\u003e\" \"\u003ccomment_box\u003e\" \"\u003cland_line\u003e\" \"\u003cmobile_number\u003e\" \"\u003caddress_alias\u003e\" \"\u003cregister_button\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "user clicks on register account",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "user must verify if the account is created successfully by viewing this \"\u003cmessage\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "line": 12,
  "name": "",
  "description": "",
  "id": "create-log-in-account-for-online-shopping;as-a-user-i-want-to-create-an-account-to-log-onto-online-shopping;",
  "rows": [
    {
      "cells": [
        "title",
        "email_address",
        "customer_firstname",
        "customer_lastname",
        "email",
        "password",
        "days",
        "months",
        "years",
        "address_firstname",
        "address_lastname",
        "address_company",
        "address_one",
        "address_two",
        "city",
        "state",
        "postcode",
        "country",
        "comment_box",
        "land_line",
        "mobile_number",
        "address_alias",
        "message"
      ],
      "line": 13,
      "id": "create-log-in-account-for-online-shopping;as-a-user-i-want-to-create-an-account-to-log-onto-online-shopping;;1"
    },
    {
      "cells": [
        "My Store",
        "ogent@net.co.za",
        "Agent",
        "Boss",
        "",
        "Fivepwd5",
        "27",
        "January",
        "1988",
        "Agent",
        "Boss",
        "Nedbank",
        "55 West Street",
        "135 Sandton",
        "Johannesburg",
        "Florida",
        "00000",
        "United States",
        "Please Help",
        "0115005331",
        "0784587458",
        "123 West",
        "Welcome to your account. Here you can manage all of your personal information and orders."
      ],
      "line": 14,
      "id": "create-log-in-account-for-online-shopping;as-a-user-i-want-to-create-an-account-to-log-onto-online-shopping;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 14,
  "name": "As a user I want to create an account to log onto online shopping",
  "description": "",
  "id": "create-log-in-account-for-online-shopping;as-a-user-i-want-to-create-an-account-to-log-onto-online-shopping;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 2,
      "name": "@regression"
    }
  ]
});
formatter.step({
  "line": 4,
  "name": "the \"My Store\" is correct",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the sign in tab",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user enters valid \"ogent@net.co.za\" address",
  "matchedColumns": [
    1
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "user clicks on create an account button",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "user enters \"Agent\" \"Boss\" \"\" \"Fivepwd5\" \"27\" \"January\" \"1988\" \"Agent\" \"Boss\" \"Nedbank\" \"55 West Street\" \"135 Sandton\" \"Johannesburg\" \"Florida\" \"00000\" \"United States\" \"Please Help\" \"0115005331\" \"0784587458\" \"123 West\" \"\u003cregister_button\u003e\"",
  "matchedColumns": [
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "user clicks on register account",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "user must verify if the account is created successfully by viewing this \"Welcome to your account. Here you can manage all of your personal information and orders.\"",
  "matchedColumns": [
    22
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "My Store",
      "offset": 5
    }
  ],
  "location": "Create_Account_steps.verify_the_url_title(String)"
});
formatter.result({
  "duration": 16835234881,
  "status": "passed"
});
formatter.match({
  "location": "Create_Account_steps.user_Clicks_On_The_SignInTab()"
});
formatter.result({
  "duration": 3376581004,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "ogent@net.co.za",
      "offset": 19
    }
  ],
  "location": "Create_Account_steps.user_Enters_ValidAddress(String)"
});
formatter.result({
  "duration": 5163714669,
  "status": "passed"
});
formatter.match({
  "location": "Create_Account_steps.user_Clicks_On_Create_An_Account_Button()"
});
formatter.result({
  "duration": 3133186088,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Agent",
      "offset": 13
    },
    {
      "val": "Boss",
      "offset": 21
    },
    {
      "val": "",
      "offset": 28
    },
    {
      "val": "Fivepwd5",
      "offset": 31
    },
    {
      "val": "27",
      "offset": 42
    },
    {
      "val": "January",
      "offset": 47
    },
    {
      "val": "1988",
      "offset": 57
    },
    {
      "val": "Agent",
      "offset": 64
    },
    {
      "val": "Boss",
      "offset": 72
    },
    {
      "val": "Nedbank",
      "offset": 79
    },
    {
      "val": "55 West Street",
      "offset": 89
    },
    {
      "val": "135 Sandton",
      "offset": 106
    },
    {
      "val": "Johannesburg",
      "offset": 120
    },
    {
      "val": "Florida",
      "offset": 135
    },
    {
      "val": "00000",
      "offset": 145
    },
    {
      "val": "United States",
      "offset": 153
    },
    {
      "val": "Please Help",
      "offset": 169
    },
    {
      "val": "0115005331",
      "offset": 183
    },
    {
      "val": "0784587458",
      "offset": 196
    },
    {
      "val": "123 West",
      "offset": 209
    },
    {
      "val": "\u003cregister_button\u003e",
      "offset": 220
    }
  ],
  "location": "Create_Account_steps.userEnters(String,String,String,String,String,String,String,String,String,String,String,String,String,String,String,String,String,String,String,String,String)"
});
formatter.result({
  "duration": 3087320385,
  "error_message": "org.openqa.selenium.WebDriverException: Unknown error (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 53 milliseconds\nBuild info: version: \u00273.4.0\u0027, revision: \u0027unknown\u0027, time: \u0027unknown\u0027\nSystem info: host: \u0027L0610033248\u0027, ip: \u002710.56.129.64\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_161\u0027\nDriver info: org.openqa.selenium.edge.EdgeDriver\nCapabilities [{applicationCacheEnabled\u003dtrue, InPrivate\u003dfalse, pageLoadStrategy\u003dnormal, platform\u003dANY, acceptSslCerts\u003dtrue, browserVersion\u003d42.17134.1.0, platformVersion\u003d10, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dMicrosoftEdge, takesScreenshot\u003dtrue, takesElementScreenshot\u003dtrue, javascriptEnabled\u003dtrue, platformName\u003dwindows}]\nSession ID: 7D7B0182-7D31-4EF3-8CFC-67A521DDE395\n*** Element info: {Using\u003did, value\u003did_gender1}\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:215)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:167)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:671)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:410)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementById(RemoteWebDriver.java:453)\r\n\tat org.openqa.selenium.By$ById.findElement(By.java:218)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:402)\r\n\tat Steps.Create_Account_steps.userEnters(Create_Account_steps.java:65)\r\n\tat ✽.Then user enters \"Agent\" \"Boss\" \"\" \"Fivepwd5\" \"27\" \"January\" \"1988\" \"Agent\" \"Boss\" \"Nedbank\" \"55 West Street\" \"135 Sandton\" \"Johannesburg\" \"Florida\" \"00000\" \"United States\" \"Please Help\" \"0115005331\" \"0784587458\" \"123 West\" \"\u003cregister_button\u003e\"(feature/web/Create_Account.feature:8)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "Create_Account_steps.user_Clicks_On_Register_Account()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Welcome to your account. Here you can manage all of your personal information and orders.",
      "offset": 73
    }
  ],
  "location": "Create_Account_steps.user_Must_Verify_If_The_Account_Is_Created_Successfully_By_Viewing_This(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.uri("feature/web/Sign_In.feature");
formatter.feature({
  "line": 1,
  "name": "Sign In shopping page",
  "description": "",
  "id": "sign-in-shopping-page",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 3,
  "name": "As a user I want to sign into online shopping page",
  "description": "",
  "id": "sign-in-shopping-page;as-a-user-i-want-to-sign-into-online-shopping-page",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "the \"\u003ctitle\u003e\" is correct",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the sign in tab",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user enters \"\u003cemail_address\u003e\" \"\u003cpassword\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "user click sign in button",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "user must verify if the account is created successfully by viewing this \"\u003cmessage\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "line": 10,
  "name": "",
  "description": "",
  "id": "sign-in-shopping-page;as-a-user-i-want-to-sign-into-online-shopping-page;",
  "rows": [
    {
      "cells": [
        "title",
        "email_address",
        "password",
        "message"
      ],
      "line": 11,
      "id": "sign-in-shopping-page;as-a-user-i-want-to-sign-into-online-shopping-page;;1"
    },
    {
      "cells": [
        "My Store",
        "ogent@net.co.za",
        "Password44",
        "Welcome to your account. Here you can manage all of your personal information and orders."
      ],
      "line": 12,
      "id": "sign-in-shopping-page;as-a-user-i-want-to-sign-into-online-shopping-page;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 12,
  "name": "As a user I want to sign into online shopping page",
  "description": "",
  "id": "sign-in-shopping-page;as-a-user-i-want-to-sign-into-online-shopping-page;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "the \"My Store\" is correct",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user clicks on the sign in tab",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user enters \"ogent@net.co.za\" \"Password44\"",
  "matchedColumns": [
    1,
    2
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "user click sign in button",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "user must verify if the account is created successfully by viewing this \"Welcome to your account. Here you can manage all of your personal information and orders.\"",
  "matchedColumns": [
    3
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "My Store",
      "offset": 5
    }
  ],
  "location": "Create_Account_steps.verify_the_url_title(String)"
});
