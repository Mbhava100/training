Feature: Update password on personal details

  Scenario Outline: As a user I want to change my password
    Given the "<title>" is correct
    When user clicks on the sign in tab
    When user enters "<email_address>" "<password>"
    When user click sign in button
    When user clicks on my personal information button
    Then user enters current "<current_password>" "<confirmation_password>""<new_password>"
    Then user clicks on save button
    And user must verify if the password is updated successfully by viewing this "<message>"

    Examples:
    |title         |email_address  |password  |current_password  |new_password|confirmation_password |message|
    |My Store      |ogent@net.co.za|Password25|Password25        |Password26  |Password26            |   Your personal information has been successfully updated  |



