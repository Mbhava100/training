Feature: Create log in account for online shopping
@regression
  Scenario Outline: As a user I want to create an account to log onto online shopping
    Given the "<title>" is correct
    When user clicks on the sign in tab
    Then user enters valid "<email_address>" address
    Then user clicks on create an account button
    Then user enters "<customer_firstname>" "<customer_lastname>" "<email>" "<password>" "<days>" "<months>" "<years>" "<address_firstname>" "<address_lastname>" "<address_company>" "<address_one>" "<address_two>" "<city>" "<state>" "<postcode>" "<country>" "<comment_box>" "<land_line>" "<mobile_number>" "<address_alias>" "<register_button>"
    And user clicks on register account
    And user must verify if the account is created successfully by viewing this "<message>"

    Examples:
    |title    |email_address  |customer_firstname|customer_lastname|email|password|days|months|years|address_firstname|address_lastname|address_company|address_one|address_two|city        |state       |postcode|country      |comment_box           |land_line    |mobile_number  |address_alias|message|
    |My Store |ogent@net.co.za|Agent             |Boss             |     |Fivepwd5|27  |January|1988              |Agent            |Boss            |Nedbank        |55 West Street    |135 Sandton|Johannesburg|Florida     | 00000   |United States | Please Help          | 0115005331     | 0784587458     | 123 West    |Welcome to your account. Here you can manage all of your personal information and orders.|


