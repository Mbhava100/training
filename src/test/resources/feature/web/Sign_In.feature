Feature: Sign In shopping page

  Scenario Outline: As a user I want to sign into online shopping page
    Given the "<title>" is correct
    When user clicks on the sign in tab
    Then user enters "<email_address>" "<password>"
    Then user click sign in button
    And user must verify if the account is created successfully by viewing this "<message>"

    Examples:
   |title         |email_address|password  |message                                                                                   |
   |My Store      |ogent@net.co.za|Password55|Welcome to your account. Here you can manage all of your personal information and orders.|



