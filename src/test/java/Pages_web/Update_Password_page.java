package Pages_web;

public class Update_Password_page {

    private String my_personal_information_xpath = "//*[@id=\"center_column\"]/div/div[1]/ul/li[4]/a/span";
    private String current_password_id = "old_passwd";
    private String new_password_id = "passwd";
    private String confirmed_password_id = "confirmation";
    private String save_button_xpath = "//*[@id=\"center_column\"]/div/form/fieldset/div[11]/button/span";
    private String message_class ="alert alert-success";


    public String getpersonal_information_xpath() {return my_personal_information_xpath;}
    public String getcurrent_password_id(){return current_password_id;}
    public String getnew_password_id() {return new_password_id;}
    public String getconfirmed_password_id() {return confirmed_password_id;}
    public String getsave_button_xpath() {return save_button_xpath;}
    public String getmessage_class() {return message_class;}

}







