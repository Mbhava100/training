package Pages_web;

public class Create_Account_page {
        // Create Account page elements
        private String email_address_id = "email_create";
        private String sign_in_xpath= "//*[@id=\"header\"]/div[2]/div/div/nav/div[1]/a";
        private String submit_create_id = "SubmitCreate";
        private String mr_id = "id_gender1";
        private String mrs_id = "id_gender2";
        private String customer_firstname_id = "customer_firstname";
        private String customer_lastname_id = "customer_lastname";
        private String email_id = "email";
        private String password_id = "passwd";
        private String days_id = "days";
        private String months_id = "months";
        private String years_id = "years";
        private String newsletter_id = "newsletter";
        private String optin_id = "optin";
        private String address_firstname_id = "firstname";
        private String address_lastname_id = "lastname";
        private String address_company_id = "company";
        private String address_one_id = "address1";
        private String address_two_id = "address2";
        private String city_id = "city";
        private String state_id = "id_state";
        private String postcode_id = "postcode";
        private String country_id = "id_country";
        private String comment_box_id = "other";
        private String landline_id = "phone";
        private String mobile_number_id = "phone_mobile";
        private String address_alias_id = "alias";
        private String register_button_id = "submitAccount";
        private String message_class = "info-account";


// Create Account get method

        public String getEmail_address_id(){return email_address_id;}
        public String getSubmit_create_id(){return submit_create_id;}
        public String getMr_id(){return mr_id;}
        public String getmrs_id(){return mrs_id;}
        public String getcustomer_firstname_id(){return customer_firstname_id;}
        public String getcustomer_lastname_id(){return customer_lastname_id;}
        public String getemail_id(){return email_id;}
        public String getpassword_id(){return password_id;}
        public String getdays_id(){return days_id;}
        public String getmonths_id(){return months_id;}
        public String getyears_id (){return years_id ;}
        public String getnewsletter_id(){return newsletter_id;}
        public String getoptin_id(){return optin_id;}
        public String getaddress_firstname_id(){return address_firstname_id;}
        public String getaddress_lastname_id (){return address_lastname_id ;}
        public String getaddress_componay_id(){return address_company_id;}
        public String getaddress_one_id(){return address_one_id;}
        public String getaddress_two_id(){return address_two_id;}
        public String getcity_id(){return city_id;}
        public String getstate_id(){return state_id;}
        public String getpostcode_id(){return postcode_id;}
        public String getcountry_id(){return country_id;}
        public String getcomment_box_id(){return comment_box_id;}
        public String getlandline_id(){return landline_id;}
        public String getmobile_number_id(){return mobile_number_id;}
        public String getaddress_alias_id(){return address_alias_id;}
        public String getregister_button_id(){return register_button_id;}
        public String getSign_in_xpath(){return sign_in_xpath;}
        public String getMessage_class(){return message_class;}

    }
