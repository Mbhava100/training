package Steps;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import za.co.nedbank.eqaframework.core.common.web.WebCommon;
import za.co.nedbank.eqaframework.core.utils.log.TestLogger;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.text.SimpleDateFormat;
import java.util.Date;
import static org.junit.Assert.assertEquals;

public abstract class Main {

    public static WebDriver wdriver;
    public String strOSName = "";
    public String strProjectLoc;

    private static final String WEBDRIVERCHROME = "webdriver.chrome.driver";
    private static final String BROWSERDRIVERS = "BrowserDrivers";
    private String driverBase = "." + File.separator + "src" + File.separator + "test" + File.separator + "resources" + File.separator + "drivers" + File.separator;
    boolean status;

    Properties prop = new Properties();


    TestLogger tLog = new TestLogger();
    WebCommon wc;

    /**
     * Description: Constructor for CommonSteps
     * Creates properties file object for use in the class
     */
    public Main() {
        InputStream input;

        try {
            // load a properties file
            strProjectLoc = System.getProperty("user.dir");
            strOSName = System.getProperty("os.name");

            System.out.println(strProjectLoc + "src" + File.separator + "test" + File.separator + "resources" + File.separator + "config" + File.separator + "config.properties");
            input = new FileInputStream(strProjectLoc + File.separator + "src" + File.separator + "test" + File.separator + "resources" + File.separator + "config" + File.separator + "config.properties");
            prop.load(input);
        } catch (FileNotFoundException e) {
            tLog.logError("File not found");
        } catch (IOException e) {
            tLog.logError("IO Exception");
        }
    }
        public void I_Open_Web_Browser_CommonStep() throws MalformedURLException {


            String strCapAppPathName = "";
            String strBrowser = "";
            String strHeadless;

            strBrowser = prop.getProperty("browser.name");
            strCapAppPathName = prop.getProperty("cap.App");
            strHeadless = prop.getProperty("browser.headless");

            //Open the desired browser
            //Browse to the relevant URL
            // ********************************************this code is for Google chrome*******************************************
            if (strBrowser.equals("GC")) {
                ChromeOptions options = new ChromeOptions();
                DesiredCapabilities cap = DesiredCapabilities.chrome();
                options.setExperimentalOption("useAutomationExtension", false);
                if (strHeadless.equals("true")) {
                    options.addArguments("headless");
                }

                if (strOSName.equalsIgnoreCase("Mac OS X")) {
                    System.setProperty(WEBDRIVERCHROME, driverBase + "chromedriver");

                    wdriver = new ChromeDriver(options);
                    wdriver.get(strCapAppPathName);
                    Dimension d = new Dimension(1400, 2000);
                    wdriver.manage().window().setSize(d);
                } else {
                    System.setProperty(WEBDRIVERCHROME, driverBase + "chromedriver.exe");

                    wdriver = new ChromeDriver(options);
                    wdriver.get(strCapAppPathName);
                    wdriver.manage().window().maximize();
                }

            }
            //*********************Edge****************************************************

            if (strBrowser.equals("EDGE")) {

                if (strOSName.equalsIgnoreCase("Mac OS X")) {
                    System.setProperty("webdriver.edge.driver", driverBase + "MicrosoftWebDriver");

                    wdriver = new EdgeDriver();
                    wdriver.get(strCapAppPathName);
                    Dimension d = new Dimension(1400, 2000);
                    wdriver.manage().window().setSize(d);
                } else {
                    System.setProperty("webdriver.edge.driver", driverBase + "MicrosoftWebDriver.exe");

                    wdriver = new EdgeDriver();
                    wdriver.get(strCapAppPathName);
                    wdriver.manage().window().maximize();
                }

            }

            // ********************************************IE*******************************************
            if (strBrowser.equals("IE")) {
                DesiredCapabilities cap = DesiredCapabilities.internetExplorer();
                cap.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);

                cap.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true);
                cap.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false);
                cap.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);


                //Catered for Windows only here.  Mac unlikely to require IE
                System.setProperty("webdriver.ie.driver", driverBase + "IEDriverServer.exe");
                wdriver = new InternetExplorerDriver(cap);
                wdriver.get(strCapAppPathName);
                wdriver.manage().window().maximize();
            }

            // ********************************************GRID*******************************************
            if (strBrowser.equals("gridGC")) {
                DesiredCapabilities cap = DesiredCapabilities.chrome();
                // Add
                tLog.logInfo("Chrome called on Selenium Grid");
                cap.setCapability("platform", "LINUX");
                cap.setCapability("version", "68.0.3440.84");
                try {
                    wdriver = new RemoteWebDriver(new URL("http://selenium.nednet.co.za/wd/hub"), cap);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

                wdriver.get(strCapAppPathName);
                //wdriver.manage().window().maximize();
                wdriver.manage().window().setSize(new Dimension(800, 600));
            }

            wc = new WebCommon(wdriver);

        }
    }
