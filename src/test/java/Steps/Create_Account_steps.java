package Steps;

import org.junit.Assert;
import Pages_web.Create_Account_page;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import cucumber.api.PendingException;
import org.openqa.selenium.support.ui.*;

import java.net.MalformedURLException;

public class Create_Account_steps extends Main {

    Create_Account_page create_account = new Create_Account_page();

    @Given("^the \"([^\"]*)\" is correct$")
    public void verify_the_url_title(String title) throws MalformedURLException, InterruptedException {
        I_Open_Web_Browser_CommonStep();

        wdriver.getTitle().equals("My Store");

//        String actual_title = title;
//        String expected_title = wdriver.getTitle();
//        System.out.println(expected_title);
//        if (actual_title.equalsIgnoreCase(expected_title)) {
//            tLog.logInfo("title matches");
//        } else {
//            System.out.println(expected_title);
//            tLog.logError("title does not match");
//        }

    }

    @When("^user clicks on the sign in tab$")
    public void user_Clicks_On_The_SignInTab() throws Throwable {
       Thread.sleep(3000);
        wdriver.findElement(By.xpath(create_account.getSign_in_xpath())).click();
    }

    @Then("^user enters valid \"([^\"]*)\" address$")
    public void user_Enters_ValidAddress(String email_address) throws Throwable {
        Thread.sleep(5000);
        wdriver.findElement(By.id(create_account.getEmail_address_id())).sendKeys(email_address);

    }

    @Then("^user clicks on create an account button$")
    public void user_Clicks_On_Create_An_Account_Button() throws Throwable {
        Thread.sleep(3000);

        wdriver.findElement(By.id(create_account.getSubmit_create_id())).click();
    }


    @Then("^user enters \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
    public void userEnters(String customer_firstname, String customer_lastname, String email, String password, String days, String months, String years, String address_firstname, String address_lastname, String address_company, String address_one, String address_two, String city, String state, String postcode, String country, String comment_box, String land_line, String mobile_number, String address_alias, String register_button) throws Throwable {
        Thread.sleep(3000);

        wdriver.findElement(By.id(create_account.getMr_id())).click();
       wdriver.findElement(By.id(create_account.getcustomer_firstname_id())).sendKeys(customer_firstname);
       wdriver.findElement(By.id(create_account.getcustomer_lastname_id())).sendKeys(customer_lastname);
       wdriver.findElement(By.id(create_account.getpassword_id())).sendKeys(password);

       Select Days = new Select(wdriver.findElement(By.id(create_account.getdays_id())));
       Days.selectByVisibleText(days);

       Select Months = new Select(wdriver.findElement(By.id(create_account.getmonths_id())));
       Months.selectByVisibleText(months);

        Thread.sleep(3000);

       Select Years = new Select(wdriver.findElement(By.id(create_account.getyears_id())));
       Years.selectByVisibleText(years);

       wdriver.findElement(By.id(create_account.getnewsletter_id())).click();
       wdriver.findElement(By.id(create_account.getoptin_id())).click();


       wdriver.findElement(By.id(create_account.getaddress_componay_id())).sendKeys(address_company);
       wdriver.findElement(By.id(create_account.getaddress_one_id())).sendKeys(address_one);
       wdriver.findElement(By.id(create_account.getaddress_two_id())).sendKeys(address_two);
       wdriver.findElement(By.id(create_account.getcity_id())).sendKeys(city);


        Thread.sleep(3000);

       Select State = new Select(wdriver.findElement(By.id(create_account.getstate_id())));
       State.selectByVisibleText(state);

       wdriver.findElement(By.id(create_account.getpostcode_id())).sendKeys(postcode);


       Select Country = new Select(wdriver.findElement(By.id(create_account.getcountry_id())));
       Country.selectByVisibleText(country);

        Thread.sleep(3000);

       wdriver.findElement(By.id(create_account.getcomment_box_id())).sendKeys(comment_box);

       wdriver.findElement(By.id(create_account.getlandline_id())).sendKeys(land_line);
        wdriver.findElement(By.id(create_account.getmobile_number_id())).sendKeys(mobile_number);
       wdriver.findElement(By.id(create_account.getaddress_alias_id())).sendKeys(address_alias);
       wdriver.findElement(By.id(create_account.getregister_button_id())).click();


    }

    @And("^user clicks on register account$")
    public void user_Clicks_On_Register_Account() throws Throwable {
        Thread.sleep(3000);

        wdriver.findElement(By.id(create_account.getregister_button_id())).click();

    }

    @And("^user must verify if the account is created successfully by viewing this \"([^\"]*)\"$")
    public void user_Must_Verify_If_The_Account_Is_Created_Successfully_By_Viewing_This(String message) throws Throwable {
        Thread.sleep(5000);

      String actual_message = wdriver.findElement(By.className(create_account.getMessage_class())).getText();
      String expected_message = message;
      Assert.assertEquals(actual_message,expected_message);
        Thread.sleep(3000);

      tLog.logInfo("client has been registered successfully");

      wdriver.quit();

    }

}

