package Steps;
import Pages_web.Sign_In_page;


import Pages_web.Update_Password_page;
import org.junit.Assert;
import Pages_web.Create_Account_page;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import cucumber.api.PendingException;
import org.openqa.selenium.support.ui.*;


    public class Sign_In_steps extends Main {


    Sign_In_page Sign_in = new Sign_In_page();




    @Then("^user enters \"([^\"]*)\" \"([^\"]*)\"$")
    public void user_Enters(String email, String passwd) throws Throwable {
        Thread.sleep(16000);

        wdriver.findElement(By.id(Sign_in.getemail_id())).sendKeys(email);

        wdriver.findElement(By.id(Sign_in.getpasswd_id())).sendKeys(passwd);

    }

    @Then("^user click sign in button$")
    public void user_Click_Sign_In_Button() throws Throwable {

        Thread.sleep(3000);

        wdriver.findElement(By.id(Sign_in.getSubmit_Login_id())).click();

    }

        }













