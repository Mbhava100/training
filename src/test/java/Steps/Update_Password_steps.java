package Steps;

import Pages_web.Update_Password_page;
import org.junit.Assert;
import Pages_web.Create_Account_page;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import cucumber.api.PendingException;
import org.openqa.selenium.support.ui.*;

import java.net.MalformedURLException;


public class Update_Password_steps extends Main {

    Update_Password_page update_password = new Update_Password_page();


    @When("^user clicks on my personal information button$")
    public void user_Clicks_On_My_Personal_Information_Button() throws Throwable {

        Thread.sleep(16000);

        wdriver.findElement(By.xpath(update_password.getpersonal_information_xpath())).click();

    }

    @Then("^user enters current \"([^\"]*)\" \"([^\"]*)\"\"([^\"]*)\"$")
    public void userEntersCurrent(String old_passwd, String passwd, String confirmation) throws Throwable {

        Thread.sleep(5000);

        wdriver.findElement(By.id(update_password.getcurrent_password_id())).sendKeys(old_passwd);
        wdriver.findElement(By.id(update_password.getnew_password_id())).sendKeys(passwd);
        wdriver.findElement(By.id(update_password.getconfirmed_password_id())).sendKeys(confirmation);

    }




        @Then("^user clicks on save button$")
        public void user_Clicks_On_Save_Button () throws Throwable {
            Thread.sleep(5000);
            wdriver.findElement(By.xpath( update_password.getsave_button_xpath())).click();
        }


        @And("^user must verify if the password is updated successfully by viewing this \"([^\"]*)\"$")
        public void user_Must_Verify_If_The_Password_Is_Updated_Successfully_By_Viewing_This (String message) throws
        Throwable {
            Thread.sleep(8000);
            String actual_message = wdriver.findElement(By.className(update_password.getmessage_class())).getText();


            String expected_message = message ;


            if(expected_message.contains(actual_message)){
                tLog.logInfo("personal information has been successfully updated.");
            }else {
                tLog.logError("personal information has not been successfully updated.");
            }


        }


    }








