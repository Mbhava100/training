package Runner_web;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.testng.annotations.BeforeTest;
import za.co.nedbank.eqaframework.core.utils.config.ApplicationConfig;

@CucumberOptions
		(
				features = "classpath:feature/web/",
				glue = {"Steps"},
				monochrome = true,
				plugin= {"pretty","html:Reports/cucumberhtmlreport",
						"com.cucumber.listener.ExtentCucumberFormatter:Output/Report.html"}
		)
public class TestRunner_Web extends AbstractTestNGCucumberTests
{
	@BeforeTest
	public void before() {
		ApplicationConfig applicationConfig = new ApplicationConfig();
		System.setProperty("logFile",applicationConfig.getLog4JFile());
	}

}
