package Runner_api;

import com.intuit.karate.junit4.Karate;
import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;


@CucumberOptions(
        features 	= "classpath:feature/api/credit_details.feature"
)

@RunWith(Karate.class)
public class Karate_TestRunner {

}
